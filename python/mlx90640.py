import sys
import smbus2
import os
import time

from mlx import MLX90640

channel = 3
address = 0x33

regStatus = 0x8000
regCtrl1  = 0x800D
regCtrl2  = 0x800E
regConfig = 0x800F

def decodeStatus(_state):
 s = {}
 s["hasData"] = _state & 8 > 0
 return s
 
#-----------------------------------------------------------
def decodeCtrl1(_state):
 s = {}
 refresh = (2 ** 9) + (2 ** 8) + (2 ** 7)
 s["chess"]   = _state & (2 ** 12) > 0
 s["refresh"] = (_state & refresh) >> 7
 s["repeat"]  = _state & (2 ** 3) > 0
 s["stepMode"]= _state & (2) > 0
 return s
 
#-----------------------------------------------------------
#def readCam(

#-----------------------------------------------------------

if __name__ == "__main__":
 #bus = smbus2.SMBus(channel)

 #regs = [regStatus, regCtrl1, regCtrl2, regConfig]
 
 #for r in regs:
  #val = bus.read_i2c_block_data(address,r,2)
  #print("0x{:02x} is {}".format(r,val))
  #status = bus.read_word_data(address,r)
  #print("0x{:02x} is {}".format(r,status))
  #time.sleep(0.1)
 #print(decodeCtrl1(status))

 #bus.close()
 
 sensor =  MLX90640(address,channel)
 
 #dis = []
 
 val = sensor.getRegs(regCtrl1,2)
 print(val)
 i = (val[0] << 8) + val[1]
 print(i)
 print(decodeCtrl1(i))
 
 #for i in range(0,32):
 # r = []
 # for j in range(0,24):
 #  r.append(sensor.getCompensatedPixData(i,j))
 # print(r)
