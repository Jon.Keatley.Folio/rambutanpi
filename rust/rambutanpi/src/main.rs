use macroquad::prelude::*;

use temp::{temp_to_color, draw_guage};

mod temp;
// https://github.com/pimoroni/hyperpixel4/issues/177
// https://shop.pimoroni.com/products/hyperpixel-4?variant=12569485443155

const SCREEN_WIDTH:i32 = 800;
const SCREEN_HEIGHT:i32 = 480;

const CAMERA_WIDTH:i32 = 32;
const CAMERA_HEIGHT:i32 = 24;
const CAMERA_PIXEL_COUNT:i32 = CAMERA_WIDTH * CAMERA_HEIGHT;

const CAMERA_LOWER:f32 = -40.;
const CAMERA_UPPER:f32 = 300.;

fn conf() -> Conf {
    Conf {
        window_title: String::from("Rambutan Pi"),
        window_width: SCREEN_WIDTH,
        window_height: SCREEN_HEIGHT,
        fullscreen: true,
        ..Default::default()
    }
}



#[macroquad::main(conf)]
async fn main() {
    
    let mut temps = vec![0f32; (CAMERA_WIDTH * CAMERA_HEIGHT) as usize];
    
    
    let guage_bounds:Rect = Rect::new(
        (SCREEN_WIDTH - 50) as f32,
        ((SCREEN_HEIGHT / 2) -40) as f32,
        40.,
        80.  
    );
    
    for x in 0..CAMERA_PIXEL_COUNT as usize
    {
        temps[x] = x as f32;
    }
    
    loop {
        
        if is_key_down(KeyCode::Escape)
        {
            break;
        }
        
        //add camera wrapper - refresh should be every 500 miliseconds
        
        let pixel_width:f32 = SCREEN_WIDTH as f32 / CAMERA_WIDTH as f32;
        let pixel_height:f32 = SCREEN_HEIGHT as f32 / CAMERA_HEIGHT as f32;
        
        clear_background(BLANK);
        
        let mut posx:f32 = 0.;
        let mut posy:f32 = 0.;
        for x in 0..CAMERA_PIXEL_COUNT as usize
        {
            draw_rectangle(
                posx,
                posy, 
                pixel_width, 
                pixel_height, 
                temp_to_color(temps[x], CAMERA_LOWER,CAMERA_UPPER)
            );
            
            posx+= pixel_width;
            if posx >=SCREEN_WIDTH as f32
            {
                posy += pixel_height;
                posx = 0.;
            }
        }
        
        draw_guage(CAMERA_LOWER,CAMERA_UPPER,guage_bounds);
        
        

        next_frame().await
    }
}