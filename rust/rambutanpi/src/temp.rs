use macroquad::color::*;
use macroquad::color::hsl_to_rgb;
use macroquad::math::Rect;
use macroquad::shapes::draw_rectangle;

pub fn temp_to_color(temp:f32, min:f32, max:f32) -> Color
{
    let t = f32::min(f32::max(min,temp),max);
    
    let normal = (t - min) / (max - min);
    hsl_to_rgb(normal,1.,0.5)
}

pub fn draw_guage(min:f32,max:f32,draw:Rect)
{
    draw_rectangle(draw.x - 1., draw.y - 1., draw.w + 2., draw.h + 2., BLACK);
    
    let range = max + min; 
    
    let size:i32 = if range >= draw.h 
    {
        1
    }
    else 
    {
        (draw.h / range) as i32
    };
    
    let steps:i32 = (draw.h / size as f32) as i32;
    
    let mut gtemp = min;
    //println!("Steps {}",range);
    for y in 0..steps
    {
        draw_rectangle(draw.x,draw.y + (y as f32),draw.w ,size as f32, temp_to_color(gtemp,min,max));
        gtemp += size as f32;
    }
    
    
}