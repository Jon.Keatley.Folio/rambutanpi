## Rust design

- [Camera](https://shop.pimoroni.com/products/mlx90640-thermal-camera-breakout?variant=12536948654163)
- [Screen](https://shop.pimoroni.com/products/hyperpixel-4?variant=12569485443155)
- [Computer - Raspberry Pi Zero](https://www.raspberrypi.com/products/raspberry-pi-zero/)
- [Camera library - mlx9064x](https://docs.rs/mlx9064x/latest/mlx9064x/index.html)
- [Graphics library - Macroquad](https://macroquad.rs/)
- [Cross compile](https://chacin.dev/blog/cross-compiling-rust-for-the-raspberry-pi)

## The plan

1. Check library works
1. Check Macroquad renders to the hyperpixel
1. Connect the two libraries
1. Sort out the CI/CD
1. Design an interface
1. Export settings to a config
